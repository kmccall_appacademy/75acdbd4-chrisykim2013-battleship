class Board
  attr_accessor :grid
  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def self.default_grid
    Array.new(10) {Array.new(10)}
  end

  def count
    count = 0
    @grid.each {|arr| arr.each {|el| count += 1 if el == :s}}
    count
  end

  def empty?(pos = nil)
    if pos == nil
      @grid.each {|arr| arr.each {|el| return false if !el.nil?}}
      true
    else
      return true if @grid[pos[0]][pos[1]].nil?
      false
    end
  end

  def full?
    @grid.each {|arr| arr.each {|el| return false if el.nil?}}
    true
  end

  def place_random_ship
    raise "Board is full!" if full?
    row_opt = (0..@grid.length - 1).to_a
    col_opt = (0..@grid.length - 1).to_a
    pos = [row_opt.sample, col_opt.sample]
    !@grid[pos[0]][pos[1]].nil? ? place_random_ship : @grid[pos[0]][pos[1]] = :s
  end

  def won?
    @grid.each {|arr| arr.each {|el| return false if el == :s}}
    true
  end

  def[](pos)
    @grid[pos[0]][pos[1]]
  end

  def populate_grid(num)
    place_random_ship until count == num
  end

  def display
    sym = [nil, :s, :x]
    @grid.each do |arr|
      arr.each {|el| sym.include?(el) ? render_id(el) : "   "}
      puts "\n"
    end
  end

  def render_id(sym)
    case sym
    when nil
      print "_  "
    when :s
      print "S  "
    when :x
      print "X  "
    end
  end
end
