require_relative 'board'
require_relative 'player'
require_relative 'ship'

class BattleshipGame
  attr_reader :player, :board, :opt_player, :opt_board
  def initialize(player, board, opt_player = nil, opt_board = nil)
    @player, @opt_player = player, opt_player
    @board, @opt_board = board, opt_board
    @current_player, @current_board, @enemy_board = @player, @board, @board
    @round = 0
  end

  def set_up
    if opt_player.nil?
      puts "How many tiny ships would you like to try to destroy?"
      ans = gets.chomp.to_i
      @enemy_board.populate_grid(ans)
    else
      @enemy_board = @opt_board
      set_ship(@current_player, @board)
      set_ship(@opt_player, @opt_board)
    end
  end

  def set_ship(player, board)
    player.get_ship_placement
    player.ship_coord.each {|el| board.grid[el[0]][el[1]] = :s}
    player.self_board.display
  end

  def play
    set_up
    until game_over?
      play_turn
      switch_players! unless @opt_player.nil?
    end
      @round += 1
      switch_players!
      puts "#{@current_player} is the winner!"
  end

  def play_turn
    att_coord = @current_player.get_play
    attack(att_coord)
    @round += 1
  end

  def attack(pos)
    puts "Hit!" if @enemy_board.grid[pos[0]][pos[1]] == :s
    @enemy_board.grid[pos[0]][pos[1]] = :x
  end

  def switch_players!
    @current_player = [@player, @opt_player][@round % 2]
    @enemy_board = [@opt_board, @board][@round % 2]
    @current_board = [@board, @opt_board][@round % 2]
  end

  def count
    @current_board.count
  end

  def game_over?
    @current_board.won? ? true : false
  end
end


if __FILE__ == $PROGRAM_NAME
  puts "Welcome to Battlefield! Please select a play option:"
  puts "pvs , pvp, or pvc"
  ans = gets.chomp
  case ans
  when "pvs"
    board = Board.new
    player = HumanPlayer.new(board)
    game = BattleshipGame.new(player,board)
    game.play
  when "pvp"
    board1 = Board.new
    player1 = HumanPlayer.new(board1)
    board2 = Board.new
    player2 = HumanPlayer.new(board2)
    game = BattleshipGame.new(player1, board1, player2, board2)
    game.play
  when "pvc"
    board1 = Board.new(Array.new(2) {Array.new(2)})
    player1 = HumanPlayer.new(board1)
    board2 = Board.new
    computer = ComputerPlayer.new(board1)
    game = BattleshipGame.new(player1, board1, computer, board2)
    game.play
  end
end
