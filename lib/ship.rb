class Ship
  def initialize(ships = Ship.default)
    @ships = ships
  end

  def self.default
    {
    # "Aircraft carrier" => 5,
    # "Battleship" => 4,
    # "Submarine" => 3,
    # "Destroyer" => 3,
    "Patrol Boat" => 2
  }
  end
end
