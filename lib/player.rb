class HumanPlayer
  attr_accessor :self_board, :ships, :ship_coord, :atk_history
  def initialize(self_board, ships = Ship.default)
    @self_board, @ships, @atk_history, @ship_coord = self_board, ships, [], []
  end

  def get_ship_placement
    puts "Follow instructions very carefully. Mistakes will restart/mess up the game!"
    @ships.each do |ship, size|
      puts "How would you like to place the #{ship}: vertical or horizontal?"
      ans = gets.chomp
      case ans
      when "vertical"
        place_vert(ship, size)
      when "horizontal"
        place_horiz(ship, size)
      end
    end
  end

  def place_head(ship)
    puts "Which coordinate would you like to place the head of the #{ship}?"
    puts "Please provide coordinates in format: X,X."
    ans = gets.chomp
    [ans[0].to_i, ans[2].to_i]
  end

  def place_vert(ship, size)
    start = place_head(ship)
    puts "Which direction would you like the ship to go: up or down?"
    direction = gets.chomp
    add_to_coord(direction, size, start)
    @ship_coord
  end

  def place_horiz(ship, size)
    start = place_head(ship)
    puts "Which direction would you like the ship to go: left or right?"
    direction = gets.chomp
    add_to_coord(direction, size, start)
    @ship_coord
  end

  def add_to_coord(direction, size, start)
    x, y = start[0], start[1]
    case direction
    when "up"
      size.times do
        gate_to_coord([x,y])
        x -= 1
      end
    when "down"
      size.times do
        gate_to_coord([x,y])
        x += 1
      end
    when "left"
      size.times do
        gate_to_coord([x,y])
        y -= 1
      end
    when "right"
      size.times do
        p [x,y]
        gate_to_coord([x,y])
        y += 1
      end
    end
  end

  def gate_to_coord(arr)
    if @ship_coord.include?(arr)
      puts "Error!"
      get_ship_placement
    else
      @ship_coord << arr
    end
  end

  def get_play
    @self_board.display
    puts "Please provide attack coordinates in the form of X,X."
    answer = gets.chomp
    coord = [answer[0].to_i, answer[2].to_i]
    if @atk_history.include?(coord)
      puts "You already chose these coordinates previously."
      get_play
    else
      @atk_history << coord
      coord
    end
  end
end

class ComputerPlayer
  attr_accessor :other_board, :self_board, :ship_lengths, :ship_coord, :history, :success, :ai_choice

  def initialize(other_board, ship_lengths = Ship.default.values)
    @other_board, @ship_lengths = other_board, ship_lengths
    @ship_coord, @history, @success = [], [], []
    @self_board = @other_board
    @ai_choice = ["up", "down", "left", "right"]
  end

  def get_play
    @success.empty? ? random_shot : smart_shot
  end

  def random_shot
    @ai_choice = ["up", "down", "left", "right"]
    grid_length = (0..@other_board.grid.length - 1).to_a
    coord = [grid_length.sample, grid_length.sample]
    id = @other_board.grid[coord[0]][coord[1]]
    if @history.include?(coord)
      random_shot
    else
      @history << coord
      @success << coord if id == :s
      coord
    end
  end

  def smart_shot
    x, y = [@success.last[0], @success.last[1]]
    move = @ai_choice.first
    case move
    when "up"
      coord = [x - 1, y]
      return smart_shot_helper(coord)
    when "down"
      coord = [x + 1, y]
      return smart_shot_helper(coord)
    when "left"
      coord = [x, y - 1]
      return smart_shot_helper(coord)
    when "right"
      coord = [x, y + 1]
      return smart_shot_helper(coord)
    end
    @success= []
    get_play
  end

  def smart_shot_helper(coord)
    if coord.any? {|el| [-1, @other_board.grid.length].include?(el)} || @history.include?(coord)
      @ai_choice.shift
      @success = [@success.first]
      smart_shot
    else
      @history << coord
      if @other_board.grid[coord[0]][coord[1]] == :s
        @success << coord
      else
        @ai_choice.shift
        @success = [@success.first]
      end
      @history.last
    end
  end

  def get_ship_placement
    opt = (0..@other_board.grid.length - 1).to_a
    choices = ["right", "down"]
    until @ship_lengths.empty?
      size = @ship_lengths.first
      x, y = opt.sample, opt.sample
      shovel = []
      choice = choices.sample
      case choice
      when "right"
        x, y = [opt.sample, opt.sample]
        size.times do
          shovel << [x, y]
          y += 1
        end
      when "down"
        x, y = [opt.sample, opt.sample]
        size.times do
          shovel << [x, y]
          x += 1
        end
      end
      # Code below adds to ship_coord even though there is recurssion. Why?
      get_ship_placement unless valid_ship?(shovel)
      # @ship_lengths.delete_at(0)
      # @ship_coord += shovel
      # p @ship_coord
      if !valid_ship?(shovel)
        get_ship_placement
      else
        @ship_lengths.delete_at(0)
        @ship_coord += shovel
      end
    end
  end

  def valid_ship?(arr)
    max_idx = @other_board.grid.length - 1
    return false if arr.empty?
    arr.each {|el| return false if @ship_coord.include?(el) || el[0] > max_idx || el[1] > max_idx}
    true
  end
end
